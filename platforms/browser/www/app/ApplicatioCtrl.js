﻿app.controller('ApplicatioCtrl', ['$rootScope', '$scope', '$http', '$location', 'ngYoutubeEmbedService', function($rootScope, $scope, $http, $location, $ngYoutubeEmbedService) {

    $scope.activechat = false;
    $scope.activevote = false;
    $scope.messages = [{
        imageyou: $rootScope.finemanimage,
        yourname: 'Fine-man',
        classname: 'botclass',
        message: 'Good day, How may i help you today?'
    }];
	$scope.checkresonse = function(){
		setTimeout( function(){ if($scope.loader ==  true){ $scope.loader = false; $rootScope.internetstatus = true; $scope.$apply();  }	 }  , 15000 );
	}
    $scope.start = 0;
    $scope.call = false;
    $scope.loader = false;
    $scope.textdata = '';
    $scope.mes = {};
    //Focus in & Out
    $scope.votingdisable = false;
    $scope.widthbox = 131;
    $scope.textboxsize = 69;
    $scope.focusin = function() {
        $scope.votingdisable = true;
        $scope.widthbox = 39;
        $scope.textboxsize = 89;
    }
    $scope.focusout = function() {
        $scope.votingdisable = false;
        $scope.widthbox = 131;
        $scope.textboxsize = 68;
    }
    $scope.focusinkey = function() {
        $scope.votingdisable = true;
        $scope.widthbox = 39;
        $scope.textboxsize = 89;
    }

    $scope.showinfobox = function() {

        $("div.infobox").slideToggle(1000);
    }

    $scope.hideinfobox = function() {
        $("div.infobox").slideToggle(1000);
    }

    $scope.getusermessage = function() {
		$('#removeaddclass').removeClass('contentarea');
		if($scope.vidoestatus == true){
			return false;
		}
        var intstatus = $rootScope.internetcheck();
        if (intstatus == false) {
            return false;
        }
       
        if ($scope.textdata !== undefined && $scope.textdata !== '') {
			$scope.activechat = true;
        	$scope.focusout();
            $scope.messages.push({
                imageyou: $rootScope.youimage,
                yourname: 'You',
                classname: 'youclass right',
                message: $scope.textdata
            });
            $("button.disableafterclick").each(function(index) {
                $(this).attr('disabled', 'disabled');
            });
            $("div.buttonattachmet").each(function(index) {
                $(this).hide(500);
            });
            $scope.get_all_news($scope.textdata);
            $scope.oldtextdata = $scope.textdata;
            $scope.textdata = '';
            console.log($scope.messages);
        }
    };
    $scope.postbacktext = '';

    $scope.showyesnow = function(mytext, ctext) {
        $scope.postbacktext = mytext;
        if (ctext !== undefined && ctext !== '') {
            $scope.messages.push({
                imageyou: $rootScope.finemanimage,
                yourname: 'Fine-man',
                classname: 'botclass',
                message: ctext,
                yesno: true
            });

        } else {
            $scope.postbackmessage();
        }
    };
    $scope.datas = {};
    $scope.get_all_news = function(textdata) {
		$('#removeaddclass').removeClass('contentarea');
        var intstatus = $rootScope.internetcheck();
        if (intstatus == false) {
            return false;
        }
		$scope.focusout();
		$scope.checkresonse();
        $scope.loader = true;
        $scope.activevote = true;
        $scope.activechat = true;
        $scope.datas.user_input = textdata;

        $scope.datas.profile_id = $rootScope.profile.userid;
		$scope.datas.sessionid = $rootScope.profile.sessionid;
        $scope.scrollchattobottom();
        $http.post($rootScope.baseurl_main + 'apiai/bot_response', $scope.datas).then(function(responsedata) {
            var response = responsedata.data;

            console.log(response.response);
            response = response.response;
            $scope.loader = false;
            $scope.buttons = [];
            $scope.urls = [];
            $scope.imgurls = [];

            if (response.fulfillment.speech == "Thank you for your feedback. I hope you have a great day!" || response.fulfillment.speech == "Thank you for your feedback. I will look at how best your need can be met and come back to you. I hope you have a great day!" || response.fulfillment.speech == "Thank you for your time today. I hope you have a great day!") {
                setTimeout(function() {
                    $scope.alertchatclose();
                }, 3000);
            }
            if (response.fulfillment.messages) {
                angular.forEach(response.fulfillment.messages, function(value, key) {

                    angular.forEach(value, function(v, k) {
                        if (k == 'payload') {
                            $scope.buttons = v.buttons;
                           
                        }

                    });
                    if (value.payload) {
                        if (value.payload.videoURL || value.payload.videoURL1) {
                            angular.forEach(value.payload, function(payloadvalue, payloadkey) {

                                $scope.urls.push({
                                    'u': payloadvalue
                                });
                            });
                        }

                        if (value.payload.imageURL) {
                            angular.forEach(value.payload, function(payloadvalue, payloadkey) {
                                $scope.imgurls.push({
                                    'link': payloadvalue
                                });
                            });
                        }
                    }
                });
            }
            if (response.fulfillment.videos) {
                angular.forEach(response.fulfillment.videos, function(payloadvalue, payloadkey) {

                    $scope.urls.push({
                        'u': payloadvalue
                    });
                });
            }

            if ($scope.buttons.length == 0 && $scope.imgurls.length == 0 && $scope.urls.length == 0) {
				if (response.fulfillment.speech == "Thank you for your feedback. I hope you have a great day!" || response.fulfillment.speech == "Thank you for your feedback. I will look at how best your need can be met and come back to you. I hope you have a great day!" || response.fulfillment.speech == "Thank you for your time today. I hope you have a great day!") {
					console.log('NO need to change text');
				}else{
	                response.fulfillment.speech = 'Hey, I still need to learn a few things before I can help you in this area. Do come back later. Thanks!';
				}
                setTimeout(function() {
                    $scope.alertchatclose();
                }, 3000);
            }
            if ($scope.buttons.length > 0) {
                $scope.poststatus = true;
                angular.forEach($scope.buttons, function(v, k) {
                    console.log(v.postback);
                    if (v.postback != 'Currently not supported by Fine-man') {
                        console.log('I AM IN');
                        $scope.poststatus = false;
                    }
                });

                if ($scope.poststatus == true) {
                    $scope.buttons = [];
                    response.fulfillment.speech = 'Hey, I still need to learn a few things before I can help you in this area. Do come back later. Thanks!';
                    setTimeout(function() {
                        $scope.alertchatclose();
                    }, 4000);
                }
            }
			if ($scope.buttons) {
				if ($scope.buttons.length == 1 && $scope.buttons[0].text == 'Autoresponse') {
					
					$scope.postback($scope.buttons[0].postback, '');
					$scope.buttons = [];
				}
				if ($scope.buttons.length == 1 && $scope.buttons[0].text == 'Ask-User') {
					
					//$scope.postback($scope.buttons[0].postback, '');
					$scope.buttons = [];
				}
				if ($scope.buttons.length == 1 && $scope.buttons[0].text == 'Close-conv') {
					
					//$scope.postback($scope.buttons[0].postback, '');
					 setTimeout(function() {
                        $scope.alertchatclose();
                    }, 4000);
					$scope.buttons = [];
					
					
				}
				
			}
            $scope.messages.push({
                imageyou: $rootScope.finemanimage,
                yourname: 'Fine-man',
                classname: 'botclass',
                message: response.fulfillment.speech,
                butt: $scope.buttons,
                imgurl: $scope.imgurls,
                url: $scope.urls
            });
            $scope.loader = false;
            $scope.scrollchattobottom();

            console.log($scope.messages);
        });
    };
    $scope.myInterval = 3000;

    $scope.postbackmessage = function() {
		$('#removeaddclass').removeClass('contentarea');
        var intstatus = $rootScope.internetcheck();
        if (intstatus == false) {
            return false;
        }
		$scope.focusout();
        $scope.activevote = true;
        $scope.loader = true;
		$scope.checkresonse();
        $scope.activechat = true;
        $scope.messages.push({
            imageyou: $rootScope.youimage,
            yourname: 'You',
            classname: 'youclass right',
            message: $scope.postbacktext

        });
        $scope.datas.user_input = $scope.postbacktext;
        $scope.datas.profile_id = $rootScope.profile.userid;
		$scope.datas.sessionid = $rootScope.profile.sessionid;
        $("button.disableafterclick").each(function(index) {
            $(this).attr('disabled', 'disabled');
        });
        $scope.scrollchattobottom();
        $http.post($rootScope.baseurl_main + 'apiai/bot_response', $scope.datas).then(function(responsedata) {
            var response = responsedata.data;
            $scope.activevote = true;
            response = response.response;
            $scope.buttons = [];
            $scope.urls = [];
            $scope.imgurls = [];

            if (response.fulfillment.messages) {
                angular.forEach(response.fulfillment.messages, function(value, key) {
                    angular.forEach(value, function(v, k) {
                        if (k == 'payload') {

                            $scope.buttons = v.buttons;
                        }
                      
                    });
                    if (value.payload) {
                        if (value.payload.videoURL || value.payload.videoURL1) {

                            angular.forEach(value.payload, function(payloadvalue, payloadkey) {
                                $scope.urls.push({
                                    'u': payloadvalue
                                });
                            });
                        }
                        if (value.payload.imageURL) {

                            angular.forEach(value.payload, function(payloadvalue, payloadkey) {
                                $scope.imgurls.push({
                                    'link': payloadvalue
                                });
                            });
                        }

                    }
                });

            }
            if (response.fulfillment.videos) {

                angular.forEach(response.fulfillment.videos, function(payloadvalue, payloadkey) {

                    $scope.urls.push({
                        'u': payloadvalue
                    });
                });
            }
            $scope.messages.push({
                imageyou: $rootScope.finemanimage,
                yourname: 'Fine-man',
                classname: 'botclass',
                message: response.fulfillment.speech,
                butt: $scope.buttons,
                imgurl: $scope.imgurls,
                url: $scope.url
            });

            console.log($scope.messages);
            $scope.loader = false;

            $scope.scrollchattobottom();

        });




    };


	$scope.videovote = function(msg, lstatus) {
        $scope.vidoestatus = false;
		if ($scope.activevote == true) {

            $scope.messages.push({
                imageyou: $rootScope.youimage,
                yourname: 'You',
                classname: 'youclass right',
                message: '',
                likestatus: lstatus,

            });
            $scope.activevote = false;
            $scope.scrollchattobottom();

        }
		
        if (msg != 'like' && msg != 'dislike') {
            $scope.postback(msg, '');
        }
		if(lstatus == 'Yes'){
			$scope.videodata = {};
			$scope.videodata.profile_id = $rootScope.profile.userid;
			$scope.videodata.sessionid = $rootScope.profile.sessionid;
			$scope.videodata.videoid = $scope.videoid;
			$http.post($rootScope.baseurl_main + 'function_call/upvote_video', $scope.videodata).then(function(responsedata) {
				console.log(responsedata);
				$scope.videodata = {};
			});
		}
		if(lstatus == 'No'){
			$scope.videodata = {};
			$scope.videodata.profile_id = $rootScope.profile.userid;
			$scope.videodata.sessionid = $rootScope.profile.sessionid;
			$scope.videodata.videoid = $scope.videoid;
			$http.post($rootScope.baseurl_main + 'function_call/downvote_video', $scope.videodata).then(function(responsedata) {
				console.log(responsedata);
				$scope.videodata = {};
			});
		}
        
    }
	
    $scope.thumbupmessage = function(msg, lstatus) {
        $scope.vidoestatus = false;
		 if ($scope.activevote == true) {
			if(lstatus == 'Yes'){
				$scope.videodata = {};
				$scope.videodata.profile_id = $rootScope.profile.userid;
				$scope.videodata.sessionid = $rootScope.profile.sessionid;

				$http.post($rootScope.baseurl_main + 'function_call/recommendation_upvote', $scope.videodata).then(function(responsedata) {
					console.log(responsedata);
					$scope.videodata = {};
				});
			}
			if(lstatus == 'No'){
				$scope.videodata = {};
				$scope.videodata.profile_id = $rootScope.profile.userid;
				$scope.videodata.sessionid = $rootScope.profile.sessionid;

				$http.post($rootScope.baseurl_main + 'function_call/recommendation_downvote', $scope.videodata).then(function(responsedata) {
					console.log(responsedata);
					$scope.videodata = {};
				});
			}
       

            $scope.messages.push({
                imageyou: $rootScope.youimage,
                yourname: 'You',
                classname: 'youclass right',
                message: '',
                likestatus: lstatus,

            });
            $scope.activevote = false;
            $scope.scrollchattobottom();

        }
        if (msg != 'like' && msg != 'dislike') {
            $scope.postback(msg, '');
        }
    }
    $scope.vidoestatus = false;
    $scope.postbackvideo = function(t1, t2) {
		$scope.videodata = {};
        $scope.vidoestatus = true;
		$scope.videodata.profile_id = $rootScope.profile.userid;
		$scope.videodata.sessionid = $rootScope.profile.sessionid;
		$scope.videodata.videoid = $scope.videoid;
		$http.post($rootScope.baseurl_main + 'function_call/video_viewed', $scope.videodata).then(function(responsedata) {
			console.log(responsedata);
			$scope.videodata = {};
		});
		$scope.postback(t1, t2);
    }
    $scope.postback = function(t1, t2) {
		$scope.focusout();
		$('#removeaddclass').removeClass('contentarea');
        var intstatus = $rootScope.internetcheck();
        if (intstatus == false) {
            return false;
        }
        $scope.activevote = true;
        $scope.activechat = true;
        $scope.loader = true;
		$scope.checkresonse();
        $("button.disableafterclick").each(function(index) {
            $(this).attr('disabled', 'disabled');
        });
        $("div.buttonattachmet").each(function(index) {
            $(this).hide(500);
        });
        if (t2 != '') {
            $scope.messages.push({
                imageyou: $rootScope.youimage,
                yourname: 'You',
                classname: 'youclass right',
                message: t2

            });

        } else {
            $('#myyoutubevideo').attr('src', '');

        }
        $scope.datas.user_input = t1;
        $scope.datas.profile_id = $rootScope.profile.userid;
		
		$scope.datas.sessionid = $rootScope.profile.sessionid;
        //For hidding Flashcart


        //  $scope.scrollchattobottom();
        $http.post($rootScope.baseurl_main + 'apiai/bot_response', $scope.datas).then(function(responsedata) {
            var response = responsedata.data;
            console.log(response.response);
            response = response.response;
            $scope.buttons = [];
            $scope.buttonsvideo = [];
            $scope.urls = [];
            $scope.imgurls = [];

            if (response.fulfillment.speech == "Thank you for your feedback. I hope you have a great day!" || response.fulfillment.speech == "Thank you for your feedback. I will look at how best your need can be met and come back to you. I hope you have a great day!" || response.fulfillment.speech == "Thank you for your time today. I hope you have a great day!") {
                setTimeout(function() {
                    $scope.alertchatclose();
                }, 4000);
            }
            if (response.fulfillment.messages) {

                angular.forEach(response.fulfillment.messages, function(value, key) {
                    angular.forEach(value, function(v, k) {
                        if (k == 'payload') {

                            $scope.buttons = v.buttons;
                            if ($scope.buttons) {
                                angular.forEach($scope.buttons, function(bv, bk) {
                                    if (bv.type == 'Voting') {
                                        $scope.buttons = [];
                                        $scope.buttonsvideo = v.buttons;
                                        $scope.focusin();
                                    }
                                })

                            }

                        }

                    });
                    if (value.payload) {
                        if (value.payload.videoURL || value.payload.videoURL1) {
                            angular.forEach(value.payload, function(payloadvalue, payloadkey) {
                                $scope.urls.push({
                                    'u': payloadvalue
                                });
                            });
                        }
                        if (value.payload.imageURL) {
                            angular.forEach(value.payload, function(payloadvalue, payloadkey) {
                                $scope.imgurls.push({
                                    'link': payloadvalue
                                });
                            });
                        }
                        //$scope.url = value.payload.videoURL;
                    }
                });

            }
            if (response.fulfillment.videos) {
                angular.forEach(response.fulfillment.videos, function(payloadvalue, payloadkey) {

                    $scope.urls.push({
                        'u': payloadvalue
                    });
                });
            }
            if ($scope.buttons.length == 0 && $scope.buttonsvideo.length == 0 && $scope.imgurls.length == 0 && $scope.urls.length == 0) {
              	if (response.fulfillment.speech == "Thank you for your feedback. I hope you have a great day!" || response.fulfillment.speech == "Thank you for your feedback. I will look at how best your need can be met and come back to you. I hope you have a great day!" || response.fulfillment.speech == "Thank you for your time today. I hope you have a great day!") {
					console.log('NO need to change text');
				}else{
	                response.fulfillment.speech = 'Hey, I still need to learn a few things before I can help you in this area. Do come back later. Thanks!';
				}
                setTimeout(function() {
                    $scope.alertchatclose();
                }, 3000);
            }


            if ($scope.buttons.length > 0) {
                $scope.poststatus = true;
                angular.forEach($scope.buttons, function(v, k) {
                    console.log(v.postback);
                    if (v.postback != 'Currently not supported by Fine-man') {
                        console.log('I AM IN');
                        $scope.poststatus = false;
                    }
                });

                if ($scope.poststatus == true) {
                    $scope.buttons = [];
                    response.fulfillment.speech = 'Hey, I still need to learn a few things before I can help you in this area. Do come back later. Thanks!';
                    setTimeout(function() {
                        $scope.alertchatclose();
                    }, 3000);
                }
            }
			if ($scope.buttons) {
				if ($scope.buttons.length == 1 && $scope.buttons[0].text == 'Autoresponse') {
					
					$scope.postback($scope.buttons[0].postback, '');
					$scope.buttons = [];
				}
				if ($scope.buttons.length == 1 && $scope.buttons[0].text == 'Ask-User') {
					
					//$scope.postback($scope.buttons[0].postback, '');
					$scope.buttons = [];
				}
				if ($scope.buttons.length == 1 && $scope.buttons[0].text == 'Close-conv') {
					
					//$scope.postback($scope.buttons[0].postback, '');
					 setTimeout(function() {
                        $scope.alertchatclose();
                    }, 4000);
					$scope.buttons = [];
					
					
				}
				
			}
            $scope.messages.push({
                imageyou: $rootScope.finemanimage,
                yourname: 'Fine-man',
                classname: 'botclass',
                message: response.fulfillment.speech,
                butt: $scope.buttons,
                buttvideo: $scope.buttonsvideo,
                imgurl: $scope.imgurls,
                url: $scope.urls
            });


            console.log($scope.messages);
            $scope.loader = false;

            $scope.scrollchattobottom();

        });




    };


    $scope.scrollchattobottom = function() {
        $('div.mydivboxscroll').animate({
            scrollTop: $('div.mydivboxscroll').prop("scrollHeight")
        }, 1000);
    };

    $scope.canclepostback = function() {
        $scope.postbacktext = $scope.oldtextdata;
        $scope.postbackmessage();
    };


    $scope.playvideo = function(url) {
			var v = url.split('=');
			$scope.videoid = v[1];
			var newurl = 'https://www.youtube.com/embed/' + v[1]+'?showinfo=0';
        $('#myyoutubevideo').attr('src', newurl);
    }
    $scope.options = {
        clicking: true,
        visible: 3,
        startSlide: 0,
        border: 0,
        dir: 'ltr',
        width: 364,
        height: 270,
        space: 220,
        controls: true,
        //autoRotationSpeed: 2500,
        loop: true
    };


    $scope.alertchatclose = function() {
		$('#removeaddclass').addClass('contentarea');
        $scope.activechat = false;
        $scope.messages = [];
        $scope.$apply();
    }




}]);




app.directive('formDrctv', function() {
    return function($scope, el, attrs) {
        $scope.$watch(attrs.name + '.$error', function(newVal, oldVal) {

            //console.log($error);      
            // console.log($scope.profiled.photoselected);


        }, true);

    }
});