app.controller('ProfileCtrl', function($rootScope, $scope, $http, $location) {

    $scope.userprofile = {};
   
    $rootScope.profile = {};
    $scope.loginvar = {};
    $scope.loginvar.userid = '';
    $scope.loginvar.password = '';
    $scope.profiled = {};
 	$scope.googlelogin =  function(){
		  
		
		
			window.plugins.googleplus.login(
				{},
				function (obj) {
					 $scope.userprofile = obj;
					 $scope.userprofile.provider = 'google';
				 	  $http.post($rootScope.baseurl_main + 'registration/registersocial', $scope.userprofile).then(function(responsedata) {
							$rootScope.loader = false;
					
						var response = responsedata.data;
						$rootScope.profile = response;
						$scope.profiled = $rootScope.profile;
						localStorage.setItem("changeai", JSON.stringify($scope.profiled));
						
						if (response.status === 'fail') {
							$scope.loginerror = response.message;
							
						}else{
							//$rootScope.socialimage = $scope.sresult.imageUrl;
							$rootScope.loginactive = true;
							
							if (response.loginfirst === "0") {
								$scope.getlocation();
								$location.path('/edit-profile');
							} else {
								$location.path('/profile');
							}
							//alert(response.status);
						}
						// $scope.siguperror ='Some error happened';
						$scope.$apply();

						});
					
				},
				function (msg) {
					alert(msg);
				 // document.querySelector("#feedback").innerHTML = "error: " + msg;
				}
				
			);
		   $scope.$apply();
	}
    $scope.register = function() {
        $rootScope.loader = true;
        $scope.userprofile.provider = 'web';
        $http.post($rootScope.baseurl_main + 'registration/register', $scope.userprofile).then(function(responsedata) {
            var response = responsedata.data;
            $rootScope.loader = false;
            if (response.status === 'fail') {
                $scope.siguperror = response.message;
            } else if (response.status === 'success') {
                $rootScope.sigupmessage = response.message;
                $location.path('/registration-success')
            }
           
        });
    }

    $scope.signup = function() {
        $location.path('/sigup')
    }
	
     $scope.fblogin = function() {
      
		FacebookInAppBrowser.login({
			send: function() {
				//alert('login opened');
			},
			success: function(access_token) {
			
				//alert('done, access token: ' + access_token);
			},
			denied: function() {
				
				//alert('user denied');
			},
			timeout: function(){
			
				//alert('a timeout has occurred, probably a bad internet connection');
			},
			complete: function(access_token) {
			//	alert(access_token);
			//	alert('window closed');
				if(access_token) {
					console.log(access_token);
				} else {
					console.log('no access token');
				}
			},
			userInfo: function(userInfo) {
				if(userInfo) {
					$scope.sresult = userInfo;
					
					$rootScope.loader = true;
					$http.post($rootScope.baseurl_main + 'registration/registersocial', $scope.sresult).then(function(responsedata) {
						$rootScope.loader = false;
					//	alert(2);
						var response = responsedata.data;
						$rootScope.profile = response;
						$scope.profiled = $rootScope.profile;
						localStorage.setItem("changeai", JSON.stringify($scope.profiled));
						if (response.status === 'fail') {
							$scope.loginerror = response.message;
						} else if (response.status === 'success') {
							$rootScope.socialimage = $scope.sresult.imageUrl;
							 $rootScope.loginactive = true;
							if (response.loginfirst === "0") {
								$scope.getlocation();
								$location.path('/edit-profile');
							} else {
								$location.path('/bot');
							}
						}
						// $scope.siguperror ='Some error happened';
						$scope.$apply();
					});
					
				} else {
					console.log('no user info');
				}
			}
		});
    } 
    $scope.$on('event:social-sign-in-success', (event, userDetails) => {
        $rootScope.loader = true;
        $scope.sresult = userDetails;
        console.log($scope.sresult);
        $rootScope.profile.userid = $scope.sresult.uid;
        $http.post($rootScope.baseurl_main + 'registration/registersocial', $scope.sresult).then(function(responsedata) {
            $rootScope.loader = false;
            var response = responsedata.data;
			$rootScope.profile = response;
		    $scope.profiled = $rootScope.profile;
			localStorage.setItem("changeai", JSON.stringify($scope.profiled));
            if (response.status === 'fail') {
                $scope.loginerror = response.message;
            } else if (response.status === 'success') {
				$rootScope.socialimage = $scope.sresult.imageUrl;
				 $rootScope.loginactive = true;
                if (response.loginfirst === "0") {
					$scope.getlocation();
                    $location.path('/edit-profile');
                } else {
					$location.path('/bot');
                }
            }
            // $scope.siguperror ='Some error happened';
        });

      //  $scope.$apply();
    })
    $scope.$on('event:social-sign-out-success', function(event, userDetails) {
        $scope.result = userDetails;
    })

   
   
    $scope.login = function() {
        var intstatus = $rootScope.internetcheck();
        if (intstatus == false) {
            return false;
        }
        $rootScope.loader = true;
        $http.post($rootScope.baseurl_main + 'registration/login', $scope.loginvar).then(function(responsedata) {
            var response = responsedata.data;
            $rootScope.loader = false;
			 $rootScope.profile = response;
			 $scope.profiled = $rootScope.profile;
			localStorage.setItem("changeai", JSON.stringify($scope.profiled));
            if (response.status === 'fail') {
                $scope.loginerror = response.message;
            } else if (response.status === 'success') {
               
               
               

                $rootScope.loginactive = true;
                if (response.loginfirst === 0) {
                   
					$scope.getlocation();
                    $location.path('/bot');

                } else {

                   	$scope.getlocation();
                    $location.path('/bot')
                }

            }
        });
    }
	
	$scope.getlocation = function(){
		 		$http.get('https://freegeoip.net/json/').then(function(locationdata) {
                        $scope.locationd = locationdata.data;
                        $scope.profiled.city = $scope.locationd.city;
                        $scope.profiled.country = $scope.locationd.country_name;
                    });
	}
	
	

});

app.controller('EditApplicatioCtrl', ['$rootScope', '$scope', '$http', '$location', 'ngYoutubeEmbedService', function($rootScope, $scope, $http, $location, $ngYoutubeEmbedService) {
	$scope.editinit = function(){
		 $rootScope.loginactive = false;
		 $scope.profiled = $rootScope.profile;
		 $scope.profiled.photoselected = '';
	}
	
	$scope.editprofile = function(){
		$scope.profiled = $rootScope.profile;
		
		$http.post($rootScope.baseurl_main + 'registration/getprofile', $scope.profiled).then(function(responsedata) {
			$scope.profiled = responsedata.data.response;
			console.log(responsedata);
			$scope.backimage = 'dist/img/maleavatarholder.png';
			
			if($scope.profiled.imgurl == "dist/img/femaleavatar.png"){
				$scope.backimage = "dist/img/femaleavatarholder.png";
			}else{
				$scope.backimage = "dist/img/maleavatarholder.png";
			}
			$scope.profiled.imgurl = $scope.backimage;
			//$scope.$apply();
		});
		console.log($scope.profiled);
	}
	$scope.avatarselect = false;
	
	$scope.showavtaroption = function() {
        $scope.avatarselect = true;
    }
	
    $scope.hideavatar = function() {
        $scope.avatarselect = false;
    }
   
    $scope.avtarselection = true;
    $scope.backimage = 'dist/img/blankimagewithoption.png';
    $scope.avatarselected = function(valueimage) {
		$scope.backimage = valueimage;
		$scope.photoselected = valueimage;
		$scope.profiled.imgurl = valueimage;
		$scope.avatarselect = false;
		$rootScope.youimage = valueimage; 
		$scope.avtarselection = false;
    }
	
	$scope.avatarselected_edit = function(valueimage) {
		$scope.backimage = valueimage;
		$scope.photoselected = valueimage;
		$scope.profiled.imgurl = valueimage;
		$scope.avatarselect = false;
		$rootScope.youimage = valueimage; 
		$scope.avtarselection = false;
    }
	
	$scope.updateerror = '';
    $scope.updateprofile = function() {
        $rootScope.loader = true;
        $http.post($rootScope.baseurl_main + 'registration/updateprofile', $scope.profiled).then(function(responsedata) {
            var response = responsedata.data;
			 $rootScope.loginactive = true;
            $rootScope.loader = false;
            if (response.status === 'fail') {
                $scope.updateerror = response.message;
            } else if (response.status === 'success') {
                $rootScope.sigupmessage = response.message;
                $location.path('/profile')
            }
            //  $scope.updateerror ='Some error happened';
        });
    }
}]);
