app.controller('supportCtrl', function($rootScope, $scope, $http, $location) {
	$scope.sendactive = true;
	$scope.supportmessage = '';
    $scope.sendmessage = function() {
		$scope.datas = {};
		var intstatus = $rootScope.internetcheck();
        if (intstatus == false) {
            return false;
        }
		$scope.loader = true;
	    $scope.datas.feedback = $scope.supportmessage;
        $scope.datas.profile_id = $rootScope.profile.userid;
		$http.post($rootScope.baseurl_main + 'function_call/user_support', $scope.datas).then(function(responsedata) {
			console.log(responsedata);
			 $scope.loader = false;
			$scope.sendactive = false;
		});
	}
		
});
