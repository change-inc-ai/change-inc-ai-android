app.controller('ProfileCtrl', function($rootScope, $scope, $http, $location) {

    $scope.userprofile = {};
   
    $rootScope.profile = {};
    $scope.loginvar = {};
    $scope.loginvar.userid = '';
    $scope.loginvar.password = '';
    $scope.profiled = {};
 	$scope.googlelogin =  function(){
		
			var intstatus = $rootScope.internetcheck();
			if (intstatus == false) {
				return false;
			}
		    $rootScope.loader = true;
			window.plugins.googleplus.login(
				{},
				function (obj) {
					  
					 $scope.userprofile = obj;
					 $scope.userprofile.provider = 'google';
					 $rootScope.loader = true;
				
				 	  $http.post($rootScope.baseurl_main + 'registration/registersocial', $scope.userprofile).then(function(responsedata) {
							$rootScope.loader = false;
					
						var response = responsedata.data;
						//  alert(JSON.stringify(response));
						$rootScope.profile = response;
						$scope.profiled = $rootScope.profile;
						localStorage.setItem("changeai", JSON.stringify($scope.profiled));
						
						if (response.status === 'fail') {
							$scope.loginerror = response.message;
							
						}else{
							
							$rootScope.loginactive = true;
							$rootScope.socialimage = $scope.profiled.imgurl;
							if (response.loginfirst == 0) {
								$scope.getlocation();
								$location.path('/edit-profile');
								
							} else {
								$location.path('/bot');
								
								
							}
							//alert(response.status);
						}
						$scope.$apply();

						});
					
				},
				function (msg) {
					
					$rootScope.loader = false;
					$location.path('/login');
					$scope.$apply();
				}
				
			);
		   $scope.$apply();
	}
    $scope.register = function() {
		var intstatus = $rootScope.internetcheck();
        if (intstatus == false) {
            return false;
        }
        
        $scope.userprofile.provider = 'web';
		if($scope.userprofile.password != $scope.userprofile.confirmpassword){
			$scope.siguperror = 'Password does not match the confirm password.';
			return false;
		}
		$rootScope.loader = true;
        $http.post($rootScope.baseurl_main + 'registration/register', $scope.userprofile).then(function(responsedata) {
            var response = responsedata.data;
            $rootScope.loader = false;
            if (response.status === 'fail') {
                $scope.siguperror = response.message;
            } else if (response.status === 'success') {
                $rootScope.sigupmessage = response.message;
                $location.path('/registration-success')
            }
           
        });
    }

    $scope.signup = function() {
        $location.path('/sigup')
    }
	
     $scope.fblogin = function() {
        var intstatus = $rootScope.internetcheck();
        if (intstatus == false) {
            return false;
        }
		FacebookInAppBrowser.login({
			send: function() {
				$rootScope.loader = true;
                 setTimeout(function(){
							 $rootScope.loader = false;
							 $location.path('/login');
					         $scope.$apply();
				 },5000);
			},
			success: function(access_token) {
				
			    $rootScope.loader = false;
				$location.path('/login');
				$scope.$apply();
			},
			denied: function() {
				$rootScope.loader = false;
				$location.path('/login');
				$scope.$apply();
			},
			timeout: function(){
			     $rootScope.loader = false;
				$location.path('/login');
				$scope.$apply();
			},
			complete: function(access_token) {
				$rootScope.loader = false;
				if(access_token) {
					console.log(access_token);
				} else {
					console.log('no access token');
				}
			},
			userInfo: function(userInfo) {
				if(userInfo) {
					$scope.userprofile = userInfo;
					$rootScope.loader = true;
					$scope.userprofile.provider = 'facebook';
					//alert(JSON.stringify($scope.userprofile));
					 $http.post($rootScope.baseurl_main + 'registration/registersocial', $scope.userprofile).then(function(responsedata) {
							$rootScope.loader = false;
						//alert(JSON.stringify(responsedata));
						var response = responsedata.data;
						$rootScope.profile = response;
						$scope.profiled = $rootScope.profile;
						//alert($scope.profiled.imgurl);
						localStorage.setItem("changeai", JSON.stringify($scope.profiled));
						if (response.status === 'fail') {
							$scope.loginerror = response.message;
						} else if (response.status === 'success') {
							$rootScope.socialimage = $scope.profiled.imgurl;
							 $rootScope.loginactive = true;
							if (response.loginfirst == 0) {
								$scope.getlocation();
								$location.path('/edit-profile');
							} else {
								$location.path('/bot');
							}
						}	
						    
					 });
					
				} else {
					console.log('no user info');
				}
			}
		});
    } 
    $scope.$on('event:social-sign-in-success', (event, userDetails) => {
        $rootScope.loader = true;
        $scope.sresult = userDetails;
        console.log($scope.sresult);
        $rootScope.profile.userid = $scope.sresult.uid;
        $http.post($rootScope.baseurl_main + 'registration/registersocial', $scope.sresult).then(function(responsedata) {
			
            $rootScope.loader = false;
            var response = responsedata.data;
			$rootScope.profile = response;
		    $scope.profiled = $rootScope.profile;
			localStorage.setItem("changeai", JSON.stringify($scope.profiled));
            if (response.status === 'fail') {
                $scope.loginerror = response.message;
            } else if (response.status === 'success') {
				$rootScope.socialimage = $scope.sresult.imageUrl;
				 $rootScope.loginactive = true;
                if (response.loginfirst === "0") {
					$scope.getlocation();
                    $location.path('/edit-profile');
                } else {
					$location.path('/bot');
                }
            }
            // $scope.siguperror ='Some error happened';
        });

      //  $scope.$apply();
    })
    $scope.$on('event:social-sign-out-success', function(event, userDetails) {
        $scope.result = userDetails;
    })

   
   
    $scope.login = function() {
        var intstatus = $rootScope.internetcheck();
        if (intstatus == false) {
            return false;
        }
        $rootScope.loader = true;
        $http.post($rootScope.baseurl_main + 'registration/login', $scope.loginvar).then(function(responsedata) {
			//alert(responsedata.status);
			if(responsedata.status == 200){
				var response = responsedata.data;
				$rootScope.loader = false;
				 $rootScope.profile = response;
				 $scope.profiled = $rootScope.profile;
				localStorage.setItem("changeai", JSON.stringify($scope.profiled));
				if (response.status === 'fail') {
					$scope.loginerror = response.message;
				} else if (response.status === 'success') {



					$rootScope.loginactive = true;
					if (response.loginfirst == 0) {
						
						$scope.getlocation();
						$location.path('/edit-profile');
                        
					} else {

						$scope.getlocation();
						$location.path('/bot')
						
					}

				}
			}else{
				$scope.loginerror = 'Some error happend';
				$rootScope.loader = false;
			}
        });
    }
	
	$scope.getlocation = function(){
	   $http.get('https://freegeoip.net/json/').then(function(locationdata) {
		   		//alert(JSON.stringify(locationdata));
		   		$scope.locationd = locationdata.data;
				$scope.profiled.city = $scope.locationd.city;
				$scope.profiled.country = $scope.locationd.country_name;
       });
	}
	
	$scope.actiavetecode = false;
	$scope.forgotpassword = function(){
		$scope.actiavetecode = true;
		// $http.post($rootScope.baseurl_main + 'registration/forgotpassword', $scope.sresult).then(function(responsedata) {
		// });
	}
	
	

});

app.controller('EditApplicatioCtrl', ['$rootScope', '$scope', '$http', '$location', 'ngYoutubeEmbedService', function($rootScope, $scope, $http, $location, $ngYoutubeEmbedService) {
	$scope.editinit = function(){
		 $rootScope.loginactive = false;
		 $scope.profiled = $rootScope.profile;
		 $scope.profiled.photoselected = '';
		 $('#Userguid').modal('show'); 
		 $rootScope.CarouselCtrl();
	}
	
	$scope.editprofile = function(){
		var intstatus = $rootScope.internetcheck();
        if (intstatus == false) {
            return false;
        }
		$scope.profiled = $rootScope.profile;
		
		$http.post($rootScope.baseurl_main + 'registration/getprofile', $scope.profiled).then(function(responsedata) {
			$scope.profiled = responsedata.data.response;
			console.log(responsedata);
			$scope.backimage = 'dist/img/maleavatarholder.png';
			
			if($scope.profiled.imgurl == "dist/img/femaleavatar.png"){
				$scope.backimage = "dist/img/femaleavatarholder.png";
			}else{
				$scope.backimage = "dist/img/maleavatarholder.png";
			}
			$scope.profiled.imgurl = $scope.backimage;
			//$scope.$apply();
		});
		console.log($scope.profiled);
	}
	$scope.avatarselect = false;
	
	$scope.showavtaroption = function() {
        $scope.avatarselect = true;
    }
	
    $scope.hideavatar = function() {
        $scope.avatarselect = false;
    }
   
    $scope.avtarselection = true;
    $scope.backimage = 'dist/img/blankimagewithoption.png';
    $scope.avatarselected = function(valueimage) {
		$scope.backimage = valueimage;
		$scope.photoselected = valueimage;
		$scope.profiled.imgurl = valueimage;
		$scope.avatarselect = false;
		$rootScope.youimage = valueimage; 
		$scope.avtarselection = false;
    }
	
	$scope.avatarselected_edit = function(valueimage) {
		$scope.backimage = valueimage;
		$scope.photoselected = valueimage;
		$scope.profiled.imgurl = valueimage;
		$scope.avatarselect = false;
		$rootScope.youimage = valueimage; 
		$scope.avtarselection = false;
    }
	
	$scope.updateerror = '';
    $scope.updateprofile = function() {
		var intstatus = $rootScope.internetcheck();
        if (intstatus == false) {
            return false;
        }
        $rootScope.loader = true;
        $http.post($rootScope.baseurl_main + 'registration/updateprofile', $scope.profiled).then(function(responsedata) {
            var response = responsedata.data;
			 $rootScope.loginactive = true;
            $rootScope.loader = false;
            if (response.status === 'fail') {
                $scope.updateerror = response.message;
            } else if (response.status === 'success') {
                $rootScope.sigupmessage = response.message;
                $location.path('/profile')
            }
            //  $scope.updateerror ='Some error happened';
        });
    }
}]); 
