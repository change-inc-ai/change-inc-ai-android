/**
 * AngularJS Tutorial 1
 * @author Nick Kaye <nick.c.kaye@gmail.com>
 */
/**
 * Main AngularJS Web Application
 */
var app = angular.module('changeincApp', [
    'ngRoute','ngYoutubeEmbed','socialLogin','ui.bootstrap','ngSanitize','angular-carousel-3d'
]);

/**
 * Configure the Routes
 */

app.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        // Home
        .when("/", {
            templateUrl: "views/users/login.html",
		    controller: "ProfileCtrl"
        })
        .when("/forgot_password",{
            templateUrl: "views/users/forgot_password.html",
            controller: "ProfileCtrl"
        }).
        when("/sigup", {
          templateUrl: "views/users/sigup.html",
          controller: "ProfileCtrl"
        }).
        when("/bot", {
          templateUrl: "views/botman/chat.html",
          controller: "ApplicatioCtrl"
         
        }).
        when("/registration-success", {
          templateUrl: "views/users/registration-success.html",
          controller: "ProfileCtrl"
          
        }).  
        when("/edit-profile", {
          templateUrl: "views/users/edit-profile.html",
          controller: "EditApplicatioCtrl"
         
        }). 	   
		when("/edit-profile2", {
          templateUrl: "views/users/edit-profile_main.html",
          controller: "EditApplicatioCtrl"
         
        }).
		when("/analytics", {
          templateUrl: "views/pages/analytics.html",
          controller: "EditApplicatioCtrl"
         
        }).
		when("/userguid",{
			 templateUrl: "views/users/userguid.html",
	         controller: "UserguidCtrl"
		}).
	    when("/about", {
          templateUrl: "views/pages/about.html",
          controller: "EditApplicatioCtrl"
         
        }).
	    when("/termandcondition", {
          templateUrl: "views/pages/termandcondition.html",
          controller: "EditApplicatioCtrl"
         
        }).
	
	    when("/support", {
          templateUrl: "views/pages/support.html",
          controller: "supportCtrl"
         
        }).
	
	    when("/credits", {
          templateUrl: "views/pages/credits.html",
          controller: "EditApplicatioCtrl"
         
        }).
	     when("/chatbot", {
          templateUrl: "views/botman/botchat.html",
          controller: "ApplicatioCtrl"
        
        })
        .otherwise("/", {
            templateUrl: "partials/404.html",
            controller: "PageCtrl"
        });
}]).run(['$rootScope', '$http', '$browser', '$timeout', "$route","$location", "$window",  function ($rootScope , $http, $browser, $timeout, $route, $location, $window) {
         $rootScope.youimage = 'dist/img/maleavatar.png';
         $rootScope.finemanimage = 'dist/img/fineman1.png'; 
         $rootScope.baseurl_main = 'http://enhance.change-inc.co/enhanceall/changeinc_ai/apicorporate/';
         $location.path('/login');
	     $rootScope.loginactive = false;
		 $rootScope.activesubmenu = false;
	     $rootScope.showsetting = function(){
				if($rootScope.activesubmenu == true){
			 	 	$rootScope.activesubmenu = false;
					$('#appinfunderline').removeClass('appinfline');
				}else{
					$rootScope.activesubmenu = true;
					$('#appinfunderline').addClass('appinfline');
				}
		 }
	     $rootScope.loader = false;
		$rootScope.hideinterneterror = function(){
				$rootScope.internetstatus = false;
				return false;
		};
		$rootScope.internetcheck = function(){
			setTimeout( function(){ 
				$rootScope.loader = false;
				if(navigator.connection.type == 'none'){
					$rootScope.internetstatus = true;
					return false;
				}	 
			}  , 15000 );
			return true;
		};
	
	   $rootScope.logout = function(){
		  //  window.plugins.googleplus.logout();	
		    $rootScope.loginactive = false;
		   $location.path('/login');
		   localStorage.setItem("changeai", '{}');
	   }
	   
	   $rootScope.backtobotman = function(){
		   $location.path('/bot');
	   }
	   
	   $rootScope.editprolink = function(){
		   $location.path('/edit-profile2');
	   }
	   	$rootScope.hidemenu = function(){
			$('#appinfunderline').removeClass('appinfline');
			 $rootScope.activesubmenu = false;
		   $('.control-sidebar-dark').removeClass('control-sidebar-open');
		}
	   
	   $rootScope.splashscreen = true;
		
	   $rootScope.setinit = function(){
		   if($rootScope.splashscreen = true){
			   
			   setTimeout(function(){ $rootScope.splashscreen = false; $('.splashcontainer').hide();  }, 3000);
		   }
		   if(localStorage.getItem("changeai")){
			   $rootScope.profile = JSON.parse(localStorage.getItem("changeai"));
			  
			   if($rootScope.profile){
				   if($rootScope.profile.userid){
					   $location.path('/bot');
					   $rootScope.loginactive = true;
				   }else{
					   window.plugins.googleplus.logout();
				   }
			   }
	   	    }
	   }
	   
	   
	   $rootScope.getactivemodule = function(){
				$http.post($rootScope.baseurl_main + 'registration/activemodule', $rootScope.profile).then(function(responsedata) {
						localStorage.setItem("changeai", JSON.stringify(responsedata.data));
						$rootScope.profile = responsedata.data;
				});
		}
	   
	   $rootScope.userguid = true;
	   $rootScope.CarouselCtrl = function(){
				    //$rootScope.myInterval = 3000;
		   			if($rootScope.userguid == true){
						$rootScope.userguid = false;
						$rootScope.slides = [
							{
								image: 'dist/img/userguid/1.jpg'
							},
							{
								image: 'dist/img/userguid/2.jpg'
							},
							{
								image: 'dist/img/userguid/3.jpg'
							},
							{
								image: 'dist/img/userguid/4.jpg'
							},
							{
								image: 'dist/img/userguid/5.jpg'
							}
						];
					}
					//$rootScope.$apply();
					
				
		}
	   
		
		
}]);
 app.config(['$locationProvider', function($locationProvider) {
    $locationProvider.hashPrefix('!');
}]);
app.config(['$qProvider', function ($qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
}]);
app.config(function(socialProvider){
    socialProvider.setGoogleKey("881461701007-5ahgarujunkhq08ajp8gs4h13b1sh4bu.apps.googleusercontent.com");
   // socialProvider.setLinkedInKey("YOUR LINKEDIN CLIENT ID");
    socialProvider.setFbKey({appId: "1589530254447572", apiVersion: "v2.10"}); 
});


